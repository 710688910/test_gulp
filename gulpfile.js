let gulp = require("gulp");
let fs = require("fs");
let del = require("del");
let exec = require("child_process").exec;
let md5 = require("gulp-md5");
let crypto = require("crypto");


/**输出default.res.json文件*/
function writeResJson() {
    let inputPath = "bin-release/resource/";
    // let outPath = "bin-release/resource/";
    let resList = fs.readdirSync(inputPath);
    let resObj = {
        "resource": [],
        "groups": []
    };

    for (let i = 0, len = resList.length; i < len; ++i) {
        let groupName = resList[i];
        let absolutePath = inputPath + groupName;
        if (fs.statSync(absolutePath).isDirectory()) {
            let groupFileList = fs.readdirSync(absolutePath);

            let tempArr = [];
            for (let j = 0, len2 = groupFileList.length; j < len2; ++j) {
                let groupName2 = groupFileList[j];
                let absolutePath2 = absolutePath + "/" + groupName2;
                resObj.resource.push(getResourceObj(groupName2, absolutePath2));

                tempArr.push(getFileReadyName(groupName2));
            }

            /**写groups的内容*/
            let groupItem = {};
            groupItem["name"] = groupName;
            groupItem["keys"] = tempArr.join();
            resObj.groups.push(groupItem);

        } else {
            resObj.resource.push(getResourceObj(groupName, absolutePath));
        }
    }

    fs.writeFileSync(`${inputPath}default.res.json`, JSON.stringify(resObj), { encoding: "utf-8" });

}


/**获取default.res.json文件里面的resource数组里面的对象*/
function getResourceObj(fileName, absolutePath) {
    let obj = {};
    obj["url"] = absolutePath.replace("bin-release/resource/", "");
    obj["type"] = getFileType(fileName);
    obj["name"] = getFileReadyName(fileName);

    if (obj["type"] === "sheet") {
        let arr = [];
        let json = JSON.parse(fs.readFileSync(absolutePath, { encoding: "utf-8" }));
        let frames = json["frames"];
        for (let key in frames) {
            if (frames.hasOwnProperty(key)) {
                arr.push(key);
            }
        }
        obj["subkeys"] = arr.join();
    }
    return obj;
}

function getFileReadyName(fileName) {
    let arr = fileName.replace(".", "_").split("_");
    console.log("arr1--------", arr);
    arr.splice(-2, 1);
    console.log("arr2--------", arr);
    return arr.join("_");

}

function getFileType(fileName) {
    let arr = fileName.split(".");
    let suffix = arr[arr.length - 1];
    switch (suffix) {
        case "jpg":
        case "png":
            return "image";

        case "json":
            return "sheet";
    }
}


async function setFileNameMd5() {
    let inputPath = "bin-release/resource_temp";
    let outPath = "bin-release/resource";

    if (fs.existsSync(outPath)) {
        await del([`${outPath}/**/*`])
    } else {
        fs.mkdirSync(outPath);
    }

    return gulp.src(`${inputPath}/**/*.*`)
        .pipe(md5(16))
        .pipe(gulp.dest(outPath));
}


/**资源图片打合图处理*/
async function mergePicture() {
    let resPath = "bin-release/resource_temp/";
    if (fs.existsSync(resPath)) {
        await del([`${resPath}**/*`])
    } else {
        fs.mkdirSync(resPath);
    }

    let inputPath = "resource/assets/";
    let pathList = fs.readdirSync(inputPath);
    for (let i = 0, len = pathList.length; i < len; ++i) {
        let groupName = pathList[i];
        let absolutePath = inputPath + groupName;
        if (fs.statSync(absolutePath).isDirectory()) {
            await mergeOneGroup(absolutePath, groupName);
        } else {
            fs.copyFileSync(absolutePath, resPath + groupName);
        }
        // console.log("文件类表名------", groupName);
    }
}


/**给一个文件夹里面的图片打包成合图*/
async function mergeOneGroup(path, groupName) {
    let subPath = "E:/gulp_test/test1222/";
    fs.mkdirSync("bin-release/resource_temp/" + groupName);

    let inputPath = `${subPath}${path}`;
    let outPath = `${subPath}bin-release/resource_temp/${groupName}/${groupName}.json`;

    /**打合图的shell命令*/
    let order = `TextureMerger -p ${inputPath} -o ${outPath}`;
    // console.log("路径----", order);

    return new Promise((resolve, reject) => {
        exec(order, (error, stdout, stder) => {
            if (error) {
                console.error("执行合图命令error---------", error);
                resolve();
            } else {
                /**重写json文件的内容*/
                modifiMergePictureJson(outPath);
                resolve();
            }
        });
    });
}

/**重新定义合图json文件的内容*/
function modifiMergePictureJson(path) {
    /**把json文件以string的方式读取*/
    let str = fs.readFileSync(path, { encoding: "utf-8" });
    /**获取合图的md5码*/
    let pngMd5 = getFileMd5(path.replace(".json", ".png"));

    let sourceObj = JSON.parse(str);
    let targetObj = {};

    /**重写json文件里面的内容*/
    targetObj["file"] = sourceObj["file"].replace(".png", `_${pngMd5}.png`);
    let targetFrames = {};

    let frames = sourceObj["frames"];
    for (let key in frames) {
        if (frames.hasOwnProperty(key)) {
            let element = frames[key];
            targetFrames[key + "_png"] = element;
        }
    }

    targetObj["frames"] = targetFrames;

    /**把修改后json文件重新写入文件*/
    fs.writeFileSync(path, JSON.stringify(targetObj), { encoding: "utf-8" });
}



/**获取文件的md5码*/
function getFileMd5(path) {
    return crypto.createHash("md5").update(fs.readFileSync(path)).digest("hex").slice(0, 16);
}

gulp.task("mergePiture", mergePicture);

gulp.task("setFileNameMd5", setFileNameMd5);

gulp.task("writeResJson", writeResJson);

gulp.task("gitPull", function (cb) {
    exec("git pull", function (err, stdout, stderr) {
        console.log("gitpull代码成功-------------------------------", stdout);
        cb(err);
    });
});


gulp.task("default", gulp.series("mergePiture", "setFileNameMd5"), function () {
    console.log("默认任务被执行");
});